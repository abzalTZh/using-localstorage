const HtmlWebpackPlugin = require("html-webpack-plugin");
const TerserPlugin = require('terser-webpack-plugin');
const path = require("path");
const { template } = require("lodash");
const CopyPlugin = require("copy-webpack-plugin");

module.exports = {
    entry: {
        email: './scripts/email-validator.js',
        joinUs: './scripts/join-us-section.js',
        main: './scripts/main.js'
    },
    output: {
        path: path.resolve(__dirname, "build"),
        filename: '[name].[hash:8].js',
        sourceMapFilename: '[name].[hash:8].map',
        chunkFilename: '[id].[hash:8].js'
    },
    optimization: {
        minimize: true,
        minimizer: [new TerserPlugin()]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: 'index.html'
        }),
        new CopyPlugin({
            patterns: [
                { from: path.resolve(__dirname, "./assets/images"), to: path.resolve(__dirname, "build") }
            ]
        })],
    devServer: {
        static: {
            directory: path.join(__dirname, "dist")
        },
        hot: true,
        port: 9000
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(mode_modeles)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            },
            {
                test: /\.css$/,
                use: [
                    {loader: 'style-loader'},
                    {loader: 'css-loader'}
                ]
            },
            {
                test: /\.(png|jpg)$/i,
                use: [
                    {loader: 'url-loader'}
                ]
            }
        ]
    }
};