import { SectionCreater } from './join-us-section.js';
import { validate } from './email-validator';
import '../styles/style.css';
import '../styles/normalize.css';

const newSection = new SectionCreater;

window.addEventListener('load', newSection.create('standard'), false);
window.addEventListener('load', () => {
  if (validate(email)) {
    emailInput.style.display = 'none';
    subscribeBtn.textContent = 'Unsubscribe';
  }
}, false);

const form = document.querySelector('form.app-form');
const emailInput = document.querySelector('input.app-form__email-input');
const email = localStorage.getItem('email');
const subscribeBtn =
  document.querySelector('button.app-section__button.app-section__button--subscribe');

form.addEventListener('submit', (e) => {
  e.preventDefault();
  localStorage.setItem('email', emailInput.value);

  if (validate(emailInput.value)) {
    if (emailInput.style.display === 'none') {
      emailInput.style.display = 'block';
      subscribeBtn.textContent = 'Subscribe';
    } else {
      emailInput.style.display = 'none';
      subscribeBtn.textContent = 'Unsubscribe';
      subscribeBtn.style.marginLeft = 0;
    }
  }
}, false);

email ? emailInput.setAttribute('value', email) : emailInput.setAttribute('value', '');